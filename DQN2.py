import random
import numpy as np
from collections import deque
#import threading

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam


import tensorflow as tf
#global graph #, model
#graph = tf.get_default_graph()
#model = Sequential()
#print("AAA: ", threading.get_ident())

GAMMA = 0.95  
LEARNING_RATE = 0.001  

MEMORY_SIZE = 1000000  
BATCH_SIZE = 20

EXPLORATION_MAX = 1.0 
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995  

USE_SUB = True


class DQNSolver:

    def __init__(self, observation_space, action_space, gamma, learning_rate, exploration_min, exploration_max, exploration_decay):
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.exploration_min = exploration_min
        self.exploration_rate = exploration_max
        self.exploration_decay = exploration_decay


        self.action_space = action_space
        self.memory = deque(maxlen=MEMORY_SIZE)
        tf.keras.backend.clear_session()
        self.graph = tf.get_default_graph() 
        with self.graph.as_default():
            self.model = Sequential()
            self.model.add(Dense(24, input_shape=(observation_space,), activation="relu"))
            self.model.add(Dense(24, activation="relu"))
            self.model.add(Dense(self.action_space, activation="linear"))
            self.model.compile(loss="mse", optimizer=Adam(lr=self.learning_rate))
        #model.summary()



    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() < self.exploration_rate:
            return random.randrange(self.action_space)
        if USE_SUB:
            q_values = self.my_predict(state)
        else:
            q_values = self.model.predict(state)
        return np.argmax(q_values[0])

    def experience_replay(self):
        if len(self.memory) < BATCH_SIZE:
            return
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, action, reward, state_next, terminal in batch:
            q_update = reward
            if not terminal:
                if USE_SUB:
                    predict = self.my_predict(state_next)
                else:
                    predict = self.model.predict(state_next)
                q_update = (reward + self.gamma * np.amax(predict[0]))
            if USE_SUB:
                q_values = self.my_predict(state)
            else:
                q_values = self.model.predict(state)
            q_values[0][action] = q_update
            with self.graph.as_default():
                self.model.fit(state, q_values, verbose=0)
        self.exploration_rate *= self.exploration_decay
        self.exploration_rate = max(self.exploration_min, self.exploration_rate)

    def my_predict(self, my_state):
        with self.graph.as_default():
            predict = self.model.predict(my_state)
        return predict
