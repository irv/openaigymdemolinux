FROM python:3.7

WORKDIR /app




RUN apt-get update && apt-get install -y \
	xvfb \
	python-opengl \
	libosmesa6-dev \
	libgl1-mesa-glx \
	libglfw3


RUN pip install --upgrade pip
#RUN pip install cogment==0.1.5
#RUN pip install matplotlib
#RUN pip install keras tensorflow sklearn pandas psutil pyglet
#RUN pip install numpy
#RUN pip install gym
#RUN pip install gym[atari]
#RUN pip install pyopengl
#RUN pip install mypy-protobuf
#RUN pip install pygame
#RUN pip install box2d-py
#RUN pip install pandas_datareader
#RUN pip install https://download.pytorch.org/whl/cpu/torch-1.0.1.post2-cp37-cp37m-linux_x86_64.whl
#RUN pip install poutyne

# Copy the current directory contents into the container at /app
COPY . /app

RUN pip install -r requirements.txt
