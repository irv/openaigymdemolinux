 #!/usr/bin/env python

from flask import Flask, request, jsonify, json, abort
from flask_cors import CORS, cross_origin

import pandas as pd
import pickle
import numpy as np
import random
from sklearn.linear_model import LinearRegression
import datetime
import csv
#import rps_pb2 as data
import base64
#from google.protobuf.timestamp_pb2 import Timestamp
from collections import deque
import ast
import time
import sys
#import threading

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

methods = ('GET', 'POST')

metric_readers = {}

global gdata_set

start_time = time.time()

'''
def monitoring_loop():
    global gdata_set
    start_time = time.time()
    #import sys
    #sys.stdout.print("hello\n")
    #sys.stdout.flush()
    while True:
        end_time = time.time()
        if end_time - start_time > 5:
            print("Reading in file")
            gdata_set = get_gdata_set()
            start_time = time.time()
'''

def get_gdata_set():
    df = pd.read_csv("./data/test.csv")

    # add correct timestamp format
    timestamp_list = df['timestamp']
    new_timestamp_list = [datetime.datetime.fromtimestamp(item) for item in timestamp_list]  # fix this /1000 in future
    df['datetime'] = new_timestamp_list


    keep_list = ['trial_id',
            'agent_0_observation_score',
            'agent_0_observation_reward',
            'agent_0_observation_trial_name',
            'agent_0_action_pickled_action',
            'agent_0_reward_value',
            'human_0_reward_value',
            'datetime']


    df = df.filter(items=keep_list)

    rename_list = {"agent_0_observation_score": "p0_score",
                      "agent_0_observation_reward": "p0_obs_reward",
                      "agent_0_action_pickled_action": "p0_action",
                       "agent_0_reward_value": "p0_reward",
                       "human_0_reward_value" : 'p1_reward',
                       "agent_0_observation_trial_name": 'trial_name'}

    df = df.rename(columns=rename_list)


    # get rid of bytes encoding and replace column
    p0_action_list = df['p0_action'].to_list()
    p0_action_list = [ pickle.loads(base64.b64decode(item)) for item in p0_action_list]
    df = df.drop(columns=['p0_action'])
    df['p0_action'] = p0_action_list

    # get rid of null entries and re-index
    df = df[~df['p0_action'].isin(['null'])]
    df = df.reset_index(drop=True)


    orig_trial_unique = df.trial_id.unique()
    new_trial_list = list(range(len(orig_trial_unique)))
    new_trialcol_list = df.trial_id.replace(
    orig_trial_unique, new_trial_list)
    df['new_trial_id'] = new_trialcol_list
    return df




print('Loading set .....')
#data_set = get_data_set()
gdata_set = get_gdata_set()

print('Data set loaded')




def add_reader(name, reader):
    metric_readers[name] = reader


@app.route('/', methods=methods)
@cross_origin()
def home():
    # uncomment print(request.headers, request.get_json())
    return 'Metrics Server'


def dataframe_to_response(target, df):
    response = []

    if df.empty:
        return response

    if isinstance(df, pd.Series):
        response.append(_series_to_response(df, target))
    elif isinstance(df, pd.DataFrame):
        for col in df:
            response.append(_series_to_response(df[col], target))
    else:
        abort(404, Exception('Received object is not a dataframe or series.'))
    return response


def dataframe_to_json_table(target, df):
    response = []

    if df.empty:
        return response

    if isinstance(df, pd.DataFrame):
        response.append({'type': 'table',
                         'columns': df.columns.map(lambda col: {"text": col}).tolist(),
                         'rows': df.where(pd.notnull(df), None).values.tolist()})
    else:
        abort(404, Exception('Received object is not a dataframe.'))

    return response


def _series_to_response(df, target):
    if df.empty:
        return {'target': '%s' % (target),
                'datapoints': []}

    sorted_df = df.dropna().sort_index()

    try:
        timestamps = (sorted_df.index.astype(pd.np.int64) //
                      10 ** 6).values.tolist()  # New pandas version
    except:
        timestamps = (sorted_df.index.astype(pd.np.int64) // 10 ** 6).tolist()

    values = sorted_df.values.tolist()

    return {'target': '%s' % (df.name),
            'datapoints': list(zip(values, timestamps))}


@app.route('/query', methods=methods)
@cross_origin(max_age=600)
def query_metrics():
    # uncomment print("first print", request.headers, request.get_json())




    req = request.get_json()

    results = []

    ts_range = {'$gt': pd.Timestamp(req['range']['from']).to_pydatetime(),
                '$lte': pd.Timestamp(req['range']['to']).to_pydatetime()}


    for target in req['targets']:
        if ':' not in target.get('target', ''):
            abort(404, Exception(
                'Target must be of type: <finder>:<metric_query>, got instead: ' + target['target']))

        # puts 'timeserie in req_type'
        req_type = target.get('type', 'timeserie')

        finder, target = target['target'].split(':', 1)
        # uncomment below for debug
        # print("finder,target", finder, '-', target)
        # print('further', target.split(','))
        query_results = metric_readers[finder](
            target, ts_range)  #, gdata_set)    # runs actual function

        # uncomment print('query_results', query_results)

        if req_type == 'table':
            # uncomment print('pre-extend', results)
            results.extend(dataframe_to_json_table(target, query_results))
        else:
            results.extend(dataframe_to_response(target, query_results))
    # uncomment print('final pre-jsonify:', results)
    # uncomment print('post jsonify:', jsonify(results))
    return jsonify(results)





def GYM_high_level_info(number, ts_range): #, gdata_set):
    global gdata_set
    df = gdata_set
    # get session final scores
    score_list = df['p0_score'].to_list()
    zero_index = [i for i,d in enumerate(score_list) if d==0]
    end_index = [index-1 for index in zero_index[1:]]
    end_index.append(len(score_list)-1)
    df = df[df.index.isin(end_index)]  # pandas df of only the final scores for each session

    # make list of pandas dataframes of unique trials
    unique_trials = df['new_trial_id'].unique().tolist()
    pandas_trial_list = []
    for i in unique_trials:   # this puts different trials in a list, but in pandas format for each entry (list of panas dataframes)
        pandas_trial_list.append(df[df['new_trial_id'].isin([i])])


    max_score = []
    avg_score = []
    for test in pandas_trial_list:
        score_list = test["p0_score"].to_list()
        max_score.append(max(score_list))
        avg_score.append(sum(score_list)/len(score_list))




    df1 = gdata_set.filter(
        ['new_trial_id', 'trial_name','trial_id', 'datetime'])
    time_diff = df1.groupby('new_trial_id').last().datetime - df1.groupby('new_trial_id').first().datetime  # get time diff
    time_diff = [i.total_seconds()
                 for i in time_diff]  # turn time diff into seconds
    df1 = df1.groupby('new_trial_id').first()  # create new group
    df1['trial_duration'] = time_diff  # add to new_group
    datetime_list = df1.datetime.tolist()
    from_time_list = [1000.0*datetime.datetime.timestamp(datetime_list[i] ) for i in range(len(datetime_list))]

    df1['from_time'] = from_time_list
    to_time_list = [1000.0*datetime.datetime.timestamp(datetime_list[i] + datetime.timedelta(
        seconds=time_diff[i]) ) for i in range(len(datetime_list))]
    df1['to_time'] = to_time_list
    df1['Max Score'] = max_score
    df1['Avg Score'] = avg_score
    return df1
add_reader('GYM_high_level_info', GYM_high_level_info)




def GYM_top_wld(svalue, ts_range):  #, gdata_set):
    global gdata_set


    trial_id = svalue
    df = gdata_set
    if trial_id != 'top':
        df = gdata_set.query("trial_id == @trial_id")
        df = df.reset_index(drop=True)

    # get session final scores
    score_list = df['p0_score'].to_list()
    zero_index = [i for i,d in enumerate(score_list) if d==0]
    end_index = [index-1 for index in zero_index[1:]]
    end_index.append(len(score_list)-1)
    df = df[df.index.isin(end_index)]  # pandas df of only the final scores for each session

    # make list of pandas dataframes of unique trials
    unique_trials = df['new_trial_id'].unique().tolist()
    pandas_trial_list = []
    for i in unique_trials:   # this puts different trials in a list, but in pandas format for each entry (list of panas dataframes)
        pandas_trial_list.append(df[df['new_trial_id'].isin([i])])




    # calculate separate regression for each trial and then combine
    for df1 in pandas_trial_list:
        #df1 = pandas_trial_list[0]
        datetime_list = df1['datetime'].tolist()
        score_regr = LinearRegression()                   # do regression
        x = np.array([(mydate - datetime_list[0]).total_seconds() for mydate in datetime_list]).reshape(-1, 1)
        y = np.array(df1['p0_score']).reshape(-1, 1)
        score_regr.fit(x, y)
        z = score_regr.predict(x)
        trial_start_list = [-10] * len(z)
        trial_start_list[0] = 5
        df1['Trial Start'] = list(trial_start_list)
        df1['Score Improvement'] = list(z.flatten())
        
    # concatenate separate pandas trials and get rid of unwanted columns
    df1 = pd.concat(pandas_trial_list)
    delete_list = ['trial_id','p0_obs_reward','p0_reward','p0_action','p1_reward','new_trial_id','trial_name','session_start','trial_start']
    df1 = df1.drop(columns=delete_list)
    df1 = df1.rename(columns={'p0_score':'Agent Score'})
    #if trial_id != 'top':
    #    df1 = df1.drop(columns=['Trial Start'])

    agent_score_list = df1['Agent Score']
    tstart_list = df1['Trial Start'].tolist()
    for i in range(len(tstart_list)):
        if tstart_list[i] == 5:
            tstart_list[i] = max(agent_score_list) + 10
    print(tstart_list)
    df1 = df1.drop(columns='Trial Start')
    df1['Trial Start'] = list(tstart_list)


    
    df1 = df1.set_index('datetime')

    return df1
add_reader('GYM_top_wld', GYM_top_wld)



def GYM_top_refresh(number, ts_range): #, gdata_set):
    global gdata_set

    from_time = 1000.0*datetime.datetime.timestamp(gdata_set['datetime'].iloc[0])
    to_time = 1000.0*datetime.datetime.timestamp(gdata_set['datetime'].iloc[-1])
    global_list = {'Hit Refresh Timelines': ['Refresh'], 'from': [from_time], 'to': [to_time]}
    df = pd.DataFrame(data=global_list)
    return df
add_reader('GYM_top_refresh', GYM_top_refresh)



def GYM_trial_stats_combined_bars(svalue, ts_range):  #, gdata_set):
    global gdata_set

    trial_id = svalue

    df = gdata_set.query("trial_id == @trial_id")
 
    score_list = df['p0_score'].to_list()
    zero_index = [i for i,d in enumerate(score_list) if d==0]
    end_index = [index-1 for index in zero_index[1:]]
    end_index.append(len(score_list)-1)
    number_of_games = len(zero_index)
    final_scores = [score_list[i] for i in end_index]
    average_score = sum(final_scores)/number_of_games
    min_score = min(final_scores)
    max_score = max(final_scores)

    global_list = {'Number of Games': [number_of_games], 'Average Score': [average_score],
                    'Max Score:':[max_score], 'Min Score':[min_score]}

    df = pd.DataFrame(data=global_list)
    return df
add_reader('GYM_trial_stats_combined_bars', GYM_trial_stats_combined_bars)




def GYM_top_trial_stats_rps_graph(svalue, ts_range):  #, gdata_set):

    global gdata_set

    trial_id = svalue
    df = gdata_set

    if trial_id != 'top':
        df = gdata_set.query("trial_id == @trial_id")

    score_list = df['p0_score'].to_list()
    zero_index = [i for i,d in enumerate(score_list) if d==0]

    # create column with sub trial inicator as = 1
    session_start_list = [-2] * len(score_list)
    for item in zero_index:
        session_start_list[item] = 2
    df['session_start'] = session_start_list

    unique_trials = df['new_trial_id'].unique().tolist()
    new_trial_id_list = df['new_trial_id'].tolist()

    trial_start_list = [-2] * len(score_list)
    for i in unique_trials:
            index = new_trial_id_list.index(i)
            trial_start_list[index] = 3
    df['trial_start'] = trial_start_list


    # delete columns that are not useful for now
    delete_list = ['p0_score','p0_reward','p1_reward','new_trial_id','trial_id']
    df = df.drop(columns=delete_list)
    rename_list = {'p0_obs_reward':'Agent Reward','p0_action':'Agent Action',
                   'session_start':'Game Start', 'trial_start':'Trial Start'}
    df = df.rename(columns=rename_list)

    # fix problem with Agent Action column by replacing
    new_agent_action_list = []
    for test in df['Agent Action'].tolist():
        new_agent_action_list.append(int(test))
    df = df.drop(columns=['Agent Action','trial_name'])
    df['Agent Action'] = new_agent_action_list


    df = df.set_index('datetime')

    return df
add_reader('GYM_top_trial_stats_rps_graph', GYM_top_trial_stats_rps_graph)



app.run(host='0.0.0.0', debug=True)

