import pandas as pd
import pickle
import ast

df = pd.read_csv("out.csv")

# delete columns that are not useful for now
delete_list = ['tick_id',
                'participant_0_observation_info',
                'participant_0_observation_observation',
               'participant_0_observation_hyperparam',
               'participant_0_observation_env_2_agent',
              'participant_0_observation_env_2_human',
              'participant_0_reward_confidence',
                'participant_1_observation_info',
                'participant_1_observation_observation',
               'participant_1_observation_hyperparam',
               'participant_1_observation_env_2_agent',
              'participant_1_observation_env_2_human',
              'participant_1_reward_confidence',
              "participant_1_observation_done",
              "participant_1_observation_score",
              "participant_1_observation_reward",
              "participant_1_action_value",
              "participant_0_observation_done"]
df = df.drop(columns=delete_list)

# rename columns to make it easier
rename_list = {"participant_0_observation_done": "p0_done",
               "participant_0_observation_score": "p0_score",
              "participant_0_observation_reward": "p0_obs_reward",
              "participant_0_action_value": "p0_action",
               "participant_0_reward_value": "p0_reward",
               "participant_1_reward_value": "p1_reward"}
df = df.rename(columns=rename_list)

# get rid of bytes encoding and replace column
p0_action_list = df['p0_action'].to_list()
p0_action_list = [ pickle.loads(ast.literal_eval(item)) for item in p0_action_list]
df = df.drop(columns=['p0_action'])
df['p0_action'] = p0_action_list

# get rid of null entries and re-index
df = df[~df['p0_action'].isin(['null'])]
df = df.reset_index(drop=True)


orig_trial_unique = df.trial_id.unique()
new_trial_list = list(range(len(orig_trial_unique)))
new_trialcol_list = df.trial_id.replace(
orig_trial_unique, new_trial_list)
df['new_trial_id'] = new_trialcol_list



print(df)